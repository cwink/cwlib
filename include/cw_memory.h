#ifndef CW_MEMORY_H
#define CW_MEMORY_H

#include "cw.h"

cw_declare cw_nodiscard int cw_call cw_memory_compare(const void *left, const void *right, cw_size size, cw_size count);
cw_declare void cw_call cw_memory_copy(void *cw_restrict destination, const void *cw_restrict source, cw_size size, cw_size count);

#endif /* CW_MEMORY_H */
