#ifndef CW_ERROR_H
#define CW_ERROR_H

#include "cw.h"

enum cw_error {
	CW_ERROR_NONE = 0,

	CW_ERROR_INVALID_ARGUMENT,
	CW_ERROR_MISSING_PARAMETER,
	CW_ERROR_UNKNOWN_ARGUMENT,

	CW_ERROR_MAX
};

cw_declare cw_nodiscard const char *cw_call cw_error_get(int error);

#endif /* CW_ERROR_H */
