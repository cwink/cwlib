#include "cw_error.h"

static const char *cw_error_lookup[CW_ERROR_MAX] = {"none", "invalid argument", "missing parameter", "unknown argument"};

const char *
cw_error_get(const int error)
{
	unsigned int i;

	cw_assert(error <= 0);
	i = error * -1;
	cw_assert(i < CW_ERROR_MAX);
	return cw_error_lookup[i];
}
