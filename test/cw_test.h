#ifndef CW_TEST_H
#define CW_TEST_H

#include <stdio.h>
#include <stdlib.h>

#include "cw_error.h"
#include "cw_options.h"
#include "cw_string.h"

#define CW_TEST_MAX_TESTS 256

#define cw_test_add(test, function, description) cw_test_add_(test, #function, description, function)
#define cw_test_assert(test, condition)          cw_test_assert_(test, __LINE__, #condition, condition)

struct cw_test {
	struct {
		void (*function[CW_TEST_MAX_TESTS])(struct cw_test *);
		cw_size length;
		const char *description[CW_TEST_MAX_TESTS], *name[CW_TEST_MAX_TESTS];
	} functions;
	int exit_code, list_tests, only_failures;
	const char *argument, *function;
};

static void cw_test_add_(struct cw_test *test, const char *name, const char *description, void (*function)(struct cw_test *));
static void cw_test_assert_(struct cw_test *test, int line, const char *message, int condition);
static void cw_test_initialize(struct cw_test *test, int argc, char **argv);
static void cw_test_run(struct cw_test *test);
static void cw_test_usage(char **argv);

static void
cw_test_add_(struct cw_test *const test, const char *const name, const char *const description, void (*const function)(struct cw_test *))
{
	cw_assert(test != cw_null && name != cw_null && function != cw_null && test->functions.length < CW_TEST_MAX_TESTS);
	test->functions.description[test->functions.length] = description;
	test->functions.name[test->functions.length] = name;
	test->functions.function[test->functions.length++] = function;
}

static void
cw_test_assert_(struct cw_test *const test, const int line, const char *const message, const int condition)
{
	cw_assert(test != cw_null && line >= 0 && message != cw_null && condition >= 0 && condition <= 1);
	if (!condition) {
		test->exit_code = EXIT_FAILURE;
		(void)fprintf(stderr, "%s:%s:%u FAIL %s.\n", __FILE__, test->function, line, message);
	} else if (!test->only_failures && condition) {
		(void)fprintf(stdout, "%s:%s:%u PASS %s.\n", __FILE__, test->function, line, message);
	}
}

static void
cw_test_initialize(struct cw_test *const test, const int argc, char **const argv)
{
	struct cw_options options;
	int error;
	const char *argument, *parameter;

	cw_assert(test != cw_null && argc > 0 && argv != cw_null);
	cw_options_initialize(&options, argc, argv, "elt:");
	test->argument = cw_null;
	test->functions.length = 0;
	test->list_tests = test->only_failures = 0;
	test->exit_code = EXIT_SUCCESS;
	test->function = cw_null;
	while ((error = cw_options_next(&options, &argument, &parameter)) > 0) {
		switch (*argument) {
		case 'e':
			test->only_failures = 1;
			break;
		case 'l':
			test->list_tests = 1;
			break;
		case 't':
			test->argument = parameter;
			break;
		}
	}
	if (error < 0) {
		(void)fprintf(stderr, "Failed to parse options due to %s.\n\n", cw_error_get(error));
		cw_test_usage(argv);
		exit(EXIT_FAILURE); /* NOLINT(concurrency-mt-unsafe) */
	}
}

static void
cw_test_run(struct cw_test *const test)
{
	cw_size i;

	if (test->list_tests) {
		(void)fprintf(stdout, "Available tests:\n\n");
		for (i = 0; i < test->functions.length; ++i)
			(void)fprintf(stdout, "\t%s\t%s\n", test->functions.name[i], test->functions.description[i]);
		return;
	}
	if (test->argument != cw_null) {
		for (i = 0; i < test->functions.length; ++i) {
			if (cw_string_compare(test->argument, test->functions.name[i]) == 0) {
				test->function = test->functions.name[i];
				test->functions.function[i](test);
				return;
			}
		}
		(void)fprintf(stderr, "Test %s does not exist.\n", test->argument);
		exit(EXIT_FAILURE); /* NOLINT(concurrency-mt-unsafe) */
	} else {
		for (i = 0; i < test->functions.length; ++i) {
			test->function = test->functions.name[i];
			test->functions.function[i](test);
		}
	}
}

static void
cw_test_usage(char **const argv)
{
	cw_assert(argv != cw_null);
	(void)fprintf(stdout, "Usage: ./%s [-e] [-l] [-t name]\n\n", *argv);
	(void)fprintf(stdout, "\tRuns test cases for %s.\n\n", *argv);
	(void)fprintf(stdout, "\toptions:\n\n");
	(void)fprintf(stdout, "\t\t-e\tOnly show failed tests.\n");
	(void)fprintf(stdout, "\t\t-l\tList available test cases.\n");
	(void)fprintf(stdout, "\t\t-t\tRuns the test with the given name.\n");
}

#endif /* CW_TEST_H */
