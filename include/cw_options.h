#ifndef CW_OPTIONS_H
#define CW_OPTIONS_H

#include "cw.h"

struct cw_options {
	int argc, index;
	const char *arguments;
	char **argv;
};

cw_declare void cw_call cw_options_initialize(struct cw_options *options, int argc, char **argv, const char *arguments);
cw_declare cw_nodiscard int cw_call cw_options_next(struct cw_options *options, const char **cw_restrict argument, const char **cw_restrict parameter);

#endif /* CW_OPTIONS_H */
