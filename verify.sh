#!/bin/sh

TARGET="tests"

C_CFLAGS="-Wall -Wextra -pedantic-errors"
CPP_CFLAGS="-Wall -Wextra -pedantic-errors -isystem /usr/include/c++/11 -isystem /usr/include/x86_64-linux-gnu/c++/11"

CFLAGS="${C_CFLAGS} -xc -std=c89" ./project.sh -acdr -s "asan" "lib" "${TARGET}" || exit 1
CFLAGS="${C_CFLAGS} -xc -std=c89" ./project.sh -cdr -s "ubsan" "lib" "${TARGET}" || exit 1
CFLAGS="${C_CFLAGS} -xc -std=c89" ./project.sh -acdlr -s "asan" "lib" "${TARGET}" || exit 1
CFLAGS="${C_CFLAGS} -xc -std=c89" ./project.sh -cdlr -s "ubsan" "lib" "${TARGET}" || exit 1

CFLAGS="${C_CFLAGS} -xc -std=c99" ./project.sh -acdr -s "asan" "lib" "${TARGET}" || exit 1
CFLAGS="${C_CFLAGS} -xc -std=c99" ./project.sh -cdr -s "ubsan" "lib" "${TARGET}" || exit 1
CFLAGS="${C_CFLAGS} -xc -std=c99" ./project.sh -acdlr -s "asan" "lib" "${TARGET}" || exit 1
CFLAGS="${C_CFLAGS} -xc -std=c99" ./project.sh -cdlr -s "ubsan" "lib" "${TARGET}" || exit 1

CFLAGS="${C_CFLAGS} -xc -std=c11" ./project.sh -acdr -s "asan" "lib" "${TARGET}" || exit 1
CFLAGS="${C_CFLAGS} -xc -std=c11" ./project.sh -cdr -s "ubsan" "lib" "${TARGET}" || exit 1
CFLAGS="${C_CFLAGS} -xc -std=c11" ./project.sh -acdlr -s "asan" "lib" "${TARGET}" || exit 1
CFLAGS="${C_CFLAGS} -xc -std=c11" ./project.sh -cdlr -s "ubsan" "lib" "${TARGET}" || exit 1

CFLAGS="${CPP_CFLAGS} -xc++ -std=c++17" ./project.sh -acdr -s "asan" "lib" "${TARGET}" || exit 1
CFLAGS="${CPP_CFLAGS} -xc++ -std=c++17" ./project.sh -cdr -s "ubsan" "lib" "${TARGET}" || exit 1
CFLAGS="${CPP_CFLAGS} -xc++ -std=c++17" ./project.sh -acdlr -s "asan" "lib" "${TARGET}" || exit 1
CFLAGS="${CPP_CFLAGS} -xc++ -std=c++17" ./project.sh -cdlr -s "ubsan" "lib" "${TARGET}" || exit 1

exit 0
