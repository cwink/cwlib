#include "cw_string.h"

#ifdef CW_CONFIG_USE_STANDARD_LIBRARY
#	include cw_libc_include(string)
#endif /* CW_CONFIG_USE_STANDARD_LIBRARY */

char *
cw_string_find_right_char(char *const string, const char character)
{
#ifndef CW_CONFIG_USE_STANDARD_LIBRARY
	char *ptr;
#endif /* CW_CONFIG_USE_STANDARD_LIBRARY */

	cw_assert(string != cw_null);
#ifndef CW_CONFIG_USE_STANDARD_LIBRARY
	for (ptr = string + cw_string_length(string); ptr >= string && *ptr != character; --ptr)
		;
	return ptr < string ? cw_null : ptr;
#else
	return cw_libc_load(strrchr)(string, character);
#endif /* CW_CONFIG_USE_STANDARD_LIBRARY */
}

const char *
cw_string_find_right_char_const(const char *const string, const char character)
{
#ifndef CW_CONFIG_USE_STANDARD_LIBRARY
	const char *ptr;
#endif /* CW_CONFIG_USE_STANDARD_LIBRARY */

	cw_assert(string != cw_null);
#ifndef CW_CONFIG_USE_STANDARD_LIBRARY
	for (ptr = string + cw_string_length(string); ptr >= string && *ptr != character; --ptr)
		;
	return ptr < string ? cw_null : ptr;
#else
	return cw_libc_load(strrchr)(string, character);
#endif /* CW_CONFIG_USE_STANDARD_LIBRARY */
}
