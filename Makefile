.POSIX:
.SUFFIXES:

.SUFFIXES: .c .json .o .x
.c.json:
	$(CLANG) $(CFLAGS) -c -MJ $@ -o /dev/null -Wno-unused-command-line-argument $<
.c.o:
	$(CC) $(CFLAGS) -c -o $@ $<
.o.x:
	$(CC) $(LDFLAGS) -o $@ $^ $(LDLIBS)
