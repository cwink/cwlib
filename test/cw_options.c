#include "cw_string.h"
#include "cw_test.h"

static void
full_test(struct cw_test *const test)
{
	struct cw_options options;
	int argc, error, triggers[3];
	const char *argument, *parameter;
	const char *argv[5];

	argc = 5;
	argv[0] = "./test.x";
	argv[1] = "-a";
	argv[2] = "-b";
	argv[3] = "tester";
	argv[4] = "-c";
	cw_options_initialize(&options, argc, cw_const_cast(char **, argv), "ab:c");
	triggers[0] = triggers[1] = triggers[2] = 0;
	while ((error = cw_options_next(&options, &argument, &parameter)) > 0) {
		switch (*argument) {
		case 'a':
			triggers[0] = 1;
			break;
		case 'b':
			cw_test_assert(test, cw_string_compare(parameter, "tester") == 0);
			triggers[1] = 1;
			break;
		case 'c':
			triggers[2] = 1;
			break;
		}
	}
	cw_test_assert(test, error >= 0 && triggers[0] && triggers[1] && triggers[2]);
}

static void
invalid_argument(struct cw_test *const test)
{
	struct cw_options options;
	int argc, error, triggers[3];
	const char *argument, *parameter;
	const char *argv[5];

	argc = 5;
	argv[0] = "./test.x";
	argv[1] = "-a";
	argv[2] = "-ba";
	argv[3] = "tester";
	argv[4] = "-c";
	cw_options_initialize(&options, argc, cw_const_cast(char **, argv), "ab:c");
	triggers[0] = triggers[1] = triggers[2] = 0;
	while ((error = cw_options_next(&options, &argument, &parameter)) > 0) {
		switch (*argument) {
		case 'a':
			triggers[0] = 1;
			break;
		case 'b':
			cw_test_assert(test, cw_string_compare(parameter, "tester") == 0);
			triggers[1] = 1;
			break;
		case 'c':
			triggers[2] = 1;
			break;
		}
	}
	cw_test_assert(test, error == -CW_ERROR_INVALID_ARGUMENT && triggers[0] && !triggers[1] && !triggers[2]);
}

static void
missing_parameter(struct cw_test *const test)
{
	struct cw_options options;
	int argc, error, triggers[3];
	const char *argument, *parameter;
	const char *argv[4];

	argc = 4;
	argv[0] = "./test.x";
	argv[1] = "-a";
	argv[2] = "-c";
	argv[3] = "-b";
	cw_options_initialize(&options, argc, cw_const_cast(char **, argv), "ab:c");
	triggers[0] = triggers[1] = triggers[2] = 0;
	while ((error = cw_options_next(&options, &argument, &parameter)) > 0) {
		switch (*argument) {
		case 'a':
			triggers[0] = 1;
			break;
		case 'b':
			cw_test_assert(test, cw_string_compare(parameter, "tester") == 0);
			triggers[1] = 1;
			break;
		case 'c':
			triggers[2] = 1;
			break;
		}
	}
	cw_test_assert(test, error == -CW_ERROR_MISSING_PARAMETER && triggers[0] && !triggers[1] && triggers[2]);
}

static void
no_arguments(struct cw_test *const test)
{
	struct cw_options options;
	int argc, error;
	const char *argument, *parameter;
	const char *argv[1];

	argc = 1;
	argv[0] = "./test.x";
	cw_options_initialize(&options, argc, cw_const_cast(char **, argv), "ab:c");
	while ((error = cw_options_next(&options, &argument, &parameter)) > 0)
		cw_test_assert(test, 0); /* Should never reach this. */
	cw_test_assert(test, error >= 0);
}

static void
no_arguments_and_parameters(struct cw_test *const test)
{
	struct cw_options options;
	int argc, error;
	const char *argument, *parameter;
	const char *argv[1];

	argc = 1;
	argv[0] = "./test.x";
	cw_options_initialize(&options, argc, cw_const_cast(char **, argv), "");
	while ((error = cw_options_next(&options, &argument, &parameter)) > 0)
		cw_test_assert(test, 0); /* Should never reach this. */
	cw_test_assert(test, error >= 0);
}

static void
no_parameters(struct cw_test *const test)
{
	struct cw_options options;
	int argc, error, triggers[3];
	const char *argument, *parameter;
	const char *argv[5];

	argc = 5;
	argv[0] = "./test.x";
	argv[1] = "-a";
	argv[2] = "-b";
	argv[3] = "tester";
	argv[4] = "-c";
	cw_options_initialize(&options, argc, cw_const_cast(char **, argv), "");
	triggers[0] = triggers[1] = triggers[2] = 0;
	while ((error = cw_options_next(&options, &argument, &parameter)) > 0) {
		switch (*argument) {
		case 'a':
			triggers[0] = 1;
			break;
		case 'b':
			cw_test_assert(test, cw_string_compare(parameter, "tester") == 0);
			triggers[1] = 1;
			break;
		case 'c':
			triggers[2] = 1;
			break;
		}
	}
	cw_test_assert(test, error == -CW_ERROR_UNKNOWN_ARGUMENT && !triggers[0] && !triggers[1] && !triggers[2]);
}

static void
unknown_argument(struct cw_test *const test)
{
	struct cw_options options;
	int argc, error, triggers[3];
	const char *argument, *parameter;
	const char *argv[5];

	argc = 5;
	argv[0] = "./test.x";
	argv[1] = "-a";
	argv[2] = "-b";
	argv[3] = "tester";
	argv[4] = "-k";
	cw_options_initialize(&options, argc, cw_const_cast(char **, argv), "ab:c");
	triggers[0] = triggers[1] = triggers[2] = 0;
	while ((error = cw_options_next(&options, &argument, &parameter)) > 0) {
		switch (*argument) {
		case 'a':
			triggers[0] = 1;
			break;
		case 'b':
			cw_test_assert(test, cw_string_compare(parameter, "tester") == 0);
			triggers[1] = 1;
			break;
		case 'c':
			triggers[2] = 1;
			break;
		}
	}
	cw_test_assert(test, error == -CW_ERROR_UNKNOWN_ARGUMENT && triggers[0] && triggers[1] && !triggers[2]);
}

static void
validate_index_1(struct cw_test *const test)
{
	struct cw_options options;
	int argc, error, triggers[3];
	const char *argument, *parameter;
	const char *argv[6];

	argc = 6;
	argv[0] = "./test.x";
	argv[1] = "-a";
	argv[2] = "-b";
	argv[3] = "tester";
	argv[4] = "-c";
	argv[5] = "target";
	cw_options_initialize(&options, argc, cw_const_cast(char **, argv), "ab:c");
	triggers[0] = triggers[1] = triggers[2] = 0;
	while ((error = cw_options_next(&options, &argument, &parameter)) > 0) {
		switch (*argument) {
		case 'a':
			triggers[0] = 1;
			break;
		case 'b':
			cw_test_assert(test, cw_string_compare(parameter, "tester") == 0);
			triggers[1] = 1;
			break;
		case 'c':
			triggers[2] = 1;
			break;
		}
	}
	cw_test_assert(test, error >= 0 && triggers[0] && triggers[1] && triggers[2]);
	cw_test_assert(test, cw_string_compare(argv[options.index], "target") == 0);
}

static void
validate_index_2(struct cw_test *const test)
{
	struct cw_options options;
	int argc, error, triggers[3];
	const char *argument, *parameter;
	const char *argv[6];

	argc = 6;
	argv[0] = "./test.x";
	argv[1] = "-a";
	argv[2] = "-b";
	argv[3] = "tester";
	argv[4] = "target";
	argv[5] = "-c";
	cw_options_initialize(&options, argc, cw_const_cast(char **, argv), "ab:c");
	triggers[0] = triggers[1] = triggers[2] = 0;
	while ((error = cw_options_next(&options, &argument, &parameter)) > 0) {
		switch (*argument) {
		case 'a':
			triggers[0] = 1;
			break;
		case 'b':
			cw_test_assert(test, cw_string_compare(parameter, "tester") == 0);
			triggers[1] = 1;
			break;
		case 'c':
			triggers[2] = 1;
			break;
		}
	}
	cw_test_assert(test, error >= 0 && triggers[0] && triggers[1] && !triggers[2]);
	cw_test_assert(test, cw_string_compare(argv[options.index], "target") == 0);
}

static void
validate_index_3(struct cw_test *const test)
{
	struct cw_options options;
	int argc, error, triggers[3];
	const char *argument, *parameter;
	const char *argv[6];

	argc = 6;
	argv[0] = "./test.x";
	argv[1] = "target";
	argv[2] = "-a";
	argv[3] = "-b";
	argv[4] = "tester";
	argv[5] = "-c";
	cw_options_initialize(&options, argc, cw_const_cast(char **, argv), "ab:c");
	triggers[0] = triggers[1] = triggers[2] = 0;
	while ((error = cw_options_next(&options, &argument, &parameter)) > 0) {
		switch (*argument) {
		case 'a':
			triggers[0] = 1;
			break;
		case 'b':
			cw_test_assert(test, cw_string_compare(parameter, "tester") == 0);
			triggers[1] = 1;
			break;
		case 'c':
			triggers[2] = 1;
			break;
		}
	}
	cw_test_assert(test, error >= 0 && !triggers[0] && !triggers[1] && !triggers[2]);
	cw_test_assert(test, cw_string_compare(argv[options.index], "target") == 0);
}

int
main(int argc, char *argv[])
{
	struct cw_test test;

	cw_test_initialize(&test, argc, argv);
	cw_test_add(&test, full_test, "A full functioning test.");
	cw_test_add(&test, invalid_argument, "A test that results in an invalid argument.");
	cw_test_add(&test, missing_parameter, "A test that results in a missing parameter.");
	cw_test_add(&test, no_arguments, "A test with no arguments, but there are parameters.");
	cw_test_add(&test, no_arguments_and_parameters, "A test with no arguments nor any parameters.");
	cw_test_add(&test, no_parameters, "A test with arguments but no parameters.");
	cw_test_add(&test, unknown_argument, "A test that results in an unknown argument.");
	cw_test_add(&test, validate_index_1, "The first test to validate indexing.");
	cw_test_add(&test, validate_index_2, "The second test to validate indexing.");
	cw_test_add(&test, validate_index_3, "The third test to validate indexing.");
	cw_test_run(&test);
	return test.exit_code;
}
