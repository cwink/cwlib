#include "cw_memory.h"

#ifdef CW_CONFIG_USE_STANDARD_LIBRARY
#	include cw_libc_include(string)
#endif /* CW_CONFIG_USE_STANDARD_LIBRARY */

int
cw_memory_compare(const void *const left, const void *const right, const cw_size size, const cw_size count)
{
#ifndef CW_CONFIG_USE_STANDARD_LIBRARY
	const cw_u8 *eptr, *lptr, *rptr;
#endif /* CW_CONFIG_USE_STANDARD_LIBRARY */

	cw_assert(left != cw_null && right != cw_null && size > 0 && count > 0);
#ifndef CW_CONFIG_USE_STANDARD_LIBRARY
	lptr = cw_static_cast(const cw_u8 *, left);
	rptr = cw_static_cast(const cw_u8 *, right);
	for (eptr = lptr + (size * count); lptr != eptr; ++lptr, ++rptr) {
		if (*lptr < *rptr)
			return -1;
		if (*lptr > *rptr)
			return 1;
	}
	return 0;
#else
	return cw_libc_load(memcmp)(left, right, size * count);
#endif /* CW_CONFIG_USE_STANDARD_LIBRARY */
}
