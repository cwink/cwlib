#include "cw_options.h"

void
cw_options_initialize(struct cw_options *const options, const int argc, char **const argv, const char *const arguments)
{
	cw_assert(options != cw_null && argc > 0 && argv != cw_null && arguments != cw_null);
	options->argv = argv;
	options->argc = argc;
	options->index = 0;
	options->arguments = arguments;
}
