#include "cw_memory.h"

#ifdef CW_CONFIG_USE_STANDARD_LIBRARY
#	include cw_libc_include(string)
#endif /* CW_CONFIG_USE_STANDARD_LIBRARY */

void
cw_memory_copy(void *const cw_restrict destination, const void *const cw_restrict source, const cw_size size, const cw_size count)
{
#ifndef CW_CONFIG_USE_STANDARD_LIBRARY
	cw_u8 *dptr;
	const cw_u8 *eptr, *sptr;
#endif /* CW_CONFIG_USE_STANDARD_LIBRARY */

	cw_assert(destination != cw_null && source != cw_null && size > 0 && count > 0);
#ifndef CW_CONFIG_USE_STANDARD_LIBRARY
	dptr = cw_static_cast(cw_u8 *, destination);
	sptr = cw_static_cast(const cw_u8 *, source);
	for (eptr = sptr + (size * count); sptr != eptr; ++sptr)
		*dptr++ = *sptr;
#else
	cw_libc_load(memcpy)(destination, source, size * count); /* NOLINT(clang-analyzer-security.insecureAPI.DeprecatedOrUnsafeBufferHandling) */
#endif /* CW_CONFIG_USE_STANDARD_LIBRARY */
}
