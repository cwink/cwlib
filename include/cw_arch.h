#ifndef CW_ARCH_H
#define CW_ARCH_H

#undef CW_ARCH_IS_BIG_ENDIAN
#define CW_ARCH_IS_LITTLE_ENDIAN
#define CW_ARCH_TYPE_FOR_SIGNED_8_BITS signed char
#define CW_ARCH_TYPE_FOR_SIGNED_16_BITS signed short
#define CW_ARCH_TYPE_FOR_SIGNED_32_BITS signed int
#define CW_ARCH_TYPE_FOR_SIGNED_64_BITS signed long
#define CW_ARCH_TYPE_FOR_UNSIGNED_8_BITS unsigned char
#define CW_ARCH_TYPE_FOR_UNSIGNED_16_BITS unsigned short
#define CW_ARCH_TYPE_FOR_UNSIGNED_32_BITS unsigned int
#define CW_ARCH_TYPE_FOR_UNSIGNED_64_BITS unsigned long
#define CW_ARCH_TYPE_FOR_SIZE unsigned long
#define CW_ARCH_TYPE_FOR_PTRDIFF signed long
#define CW_ARCH_TYPE_FOR_SPTR signed long
#define CW_ARCH_TYPE_FOR_UPTR unsigned long

#endif /* CW_ARCH_H */
