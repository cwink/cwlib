#ifndef CW_CONFIG_H
#define CW_CONFIG_H

#ifndef CW_CONFIG_HAVE_C_11
#	if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 201112L
#		define CW_CONFIG_HAVE_C_11
#	endif
#endif /* CW_CONFIG_HAVE_C_11 */

#ifndef CW_CONFIG_HAVE_C_17
#	if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 201710L
#		define CW_CONFIG_HAVE_C_17
#	endif
#endif /* CW_CONFIG_HAVE_C_17 */

#ifndef CW_CONFIG_HAVE_C_99
#	if defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#		define CW_CONFIG_HAVE_C_99
#	endif
#endif /* CW_CONFIG_HAVE_C_99 */

#ifndef CW_CONFIG_HAVE_CPP_11
#	if defined(__cplusplus) && __cplusplus >= 201103L
#		define CW_CONFIG_HAVE_CPP_11
#	endif
#endif /* CW_CONFIG_HAVE_CPP_11 */

#ifndef CW_CONFIG_HAVE_CPP_14
#	if defined(__cplusplus) && __cplusplus >= 201402L
#		define CW_CONFIG_HAVE_CPP_14
#	endif
#endif /* CW_CONFIG_HAVE_CPP_14 */

#ifndef CW_CONFIG_HAVE_CPP_17
#	if defined(__cplusplus) && __cplusplus >= 201703L
#		define CW_CONFIG_HAVE_CPP_17
#	endif
#endif /* CW_CONFIG_HAVE_CPP_14 */

#ifndef CW_CONFIG_HAVE_CPP_98
#	if defined(__cplusplus) && __cplusplus >= 199711L
#		define CW_CONFIG_HAVE_CPP_98
#	endif
#endif /* CW_CONFIG_HAVE_CPP_98 */

#ifndef CW_CONFIG_USING_COMPILER_GNU
#	ifdef __GNUC__
#		define CW_CONFIG_USING_COMPILER_GNU
#	endif
#endif /* CW_CONFIG_USING_COMPILER_GNU */

#ifndef CW_CONFIG_USING_COMPILER_MSVC
#	ifdef _MSC_VER
#		define CW_CONFIG_USING_COMPILER_MSVC
#	endif
#endif /* CW_CONFIG_USING_COMPILER_MSVC */

#ifndef CW_CONFIG_USING_LANGUAGE_C
#	ifndef __cplusplus
#		define CW_CONFIG_USING_LANGUAGE_C
#	endif
#endif /* CW_CONFIG_USING_LANGUAGE_C */

#ifndef CW_CONFIG_USING_LANGUAGE_CPP
#	ifdef __cplusplus
#		define CW_CONFIG_USING_LANGUAGE_CPP
#	endif
#endif /* CW_CONFIG_USING_LANGUAGE_CPP */

#ifndef CW_CONFIG_USING_OS_APPLE
#	if defined(__APPLE__) || defined(__MACH__)
#		define CW_CONFIG_USING_OS_APPLE
#	endif
#endif /* CW_CONFIG_USING_OS_APPLE */

#ifndef CW_CONFIG_USING_OS_UNIX
#	if defined(__ANDROID__) || defined(__linux__) || defined(__unix__) || defined(BSD)
#		define CW_CONFIG_USING_OS_UNIX
#	endif
#endif /* CW_CONFIG_USING_OS_UNIX */

#ifndef CW_CONFIG_USING_OS_WINDOWS
#	if defined(_WIN32) || defined(_WIN64)
#		define CW_CONFIG_USING_OS_WINDOWS
#	endif
#endif /* CW_CONFIG_USING_OS_WINDOWS */

#endif /* CW_CONFIG_H */
