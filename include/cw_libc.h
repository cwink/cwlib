#ifndef CW_LIBC_H
#define CW_LIBC_H

#include "cw_config.h"

#ifndef cw_libc_include
#	ifdef CW_CONFIG_USE_STANDARD_LIBRARY
#		ifdef CW_CONFIG_USING_LANGUAGE_CPP
#			define cw_libc_include(name) <c##name>
#		else
#			define cw_libc_include(name) <name.h> /* NOLINT(bugprone-macro-parentheses) */
#		endif
#	endif
#endif /* cw_libc_include */

#ifndef cw_libc_load
#	ifdef CW_CONFIG_USE_STANDARD_LIBRARY
#		ifdef CW_CONFIG_USING_LANGUAGE_CPP
#			define cw_libc_load(name) std::name
#		else
#			define cw_libc_load(name) name
#		endif
#	endif
#endif /* cw_libc_load */

#endif /* CW_LIBC_H */
