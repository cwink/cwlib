#include "cw_memory.h"
#include "cw_test.h"

static void
compare(struct cw_test *const test)
{
	const int a[4] = {-1, -2, -3, -4};
	const int b[4] = {-1, -2, -3, -4};
	const int c[4] = {-2, -2, -3, -4};

	cw_test_assert(test, cw_memory_compare(a, b, sizeof(int), 4) == 0);
	cw_test_assert(test, cw_memory_compare(b, a, sizeof(int), 4) == 0);
	cw_test_assert(test, cw_memory_compare(a, c, sizeof(int), 4) > 0);
	cw_test_assert(test, cw_memory_compare(c, a, sizeof(int), 4) < 0);
}

static void
copy(struct cw_test *const test)
{
	const int a[4] = {-1, -2, -3, -4};
	int b[4] = {0, 0, 0, 0};

	cw_memory_copy(b, a, sizeof(int), 1);
	cw_test_assert(test, b[0] == -1 && b[1] == 0 && b[2] == 0 && b[3] == 0);
	b[0] = b[1] = b[2] = b[3] = 0;
	cw_memory_copy(b, a, sizeof(int), 2);
	cw_test_assert(test, b[0] == -1 && b[1] == -2 && b[2] == 0 && b[3] == 0);
	b[0] = b[1] = b[2] = b[3] = 0;
	cw_memory_copy(b, a, sizeof(int), 3);
	cw_test_assert(test, b[0] == -1 && b[1] == -2 && b[2] == -3 && b[3] == 0);
	b[0] = b[1] = b[2] = b[3] = 0;
	cw_memory_copy(b, a, sizeof(int), 4);
	cw_test_assert(test, b[0] == -1 && b[1] == -2 && b[2] == -3 && b[3] == -4);
}

int
main(int argc, char *argv[])
{
	struct cw_test test;

	cw_test_initialize(&test, argc, argv);
	cw_test_add(&test, compare, "Test for cw_memory_compare.");
	cw_test_add(&test, copy, "Test for cw_memory_copy.");
	cw_test_run(&test);
	return test.exit_code;
}
