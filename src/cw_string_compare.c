#include "cw_string.h"

#ifdef CW_CONFIG_USE_STANDARD_LIBRARY
#	include cw_libc_include(string)
#endif /* CW_CONFIG_USE_STANDARD_LIBRARY */

int
cw_string_compare(const char *left, const char *right)
{
	cw_assert(left != cw_null && right != cw_null);

#ifndef CW_CONFIG_USE_STANDARD_LIBRARY
	for (; *left != '\0' && *right != '\0'; ++left, ++right) {
		if (*left < *right)
			return -1;
		if (*left > *right)
			return 1;
	}
	if (*left < *right)
		return -1;
	if (*left > *right)
		return 1;
	return 0;
#else
	return cw_libc_load(strcmp)(left, right);
#endif /* CW_CONFIG_USE_STANDARD_LIBRARY */
}
