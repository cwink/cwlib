#ifndef CW_H
#define CW_H

#include "cw_types.h"

#ifndef cw_assert
#	ifdef CW_CONFIG_ENABLE_ASSERTIONS
#		if defined(CW_CONFIG_USE_STANDARD_LIBRARY)
#			undef NDEBUG
#			include cw_libc_include(assert)
#			define cw_assert assert
#		elif defined(CW_CONFIG_USING_COMPILER_GNU)
#			define cw_assert(condition)                                                                                                                               \
				if (!(condition))                                                                                                                                  \
				__builtin_trap()
#		elif defined(CW_CONFIG_USING_COMPILER_MSVC)
#			define cw_assert(condition)                                                                                                                               \
				if (!(condition))                                                                                                                                  \
				__debugbreak()
#		else
#			define cw_assert(condition)                                                                                                                               \
				if (!(condition))                                                                                                                                  \
				*cw_static_cast(volatile int *, 0) = 0
#		endif
#	else
#		define cw_assert(condition)
#	endif
#endif /* cw_assert */

#ifndef cw_call
#	if defined(CW_CONFIG_USING_OS_WINDOWS) && !defined(CW_CONFIG_USING_COMPILER_GNU)
#		define cw_call __cdecl
#	else
#		define cw_call
#	endif
#endif /* cw_call */

#ifndef cw_const_cast
#	ifdef CW_CONFIG_USING_LANGUAGE_CPP
#		define cw_const_cast(type, expression) const_cast<type>(expression)
#	else
#		define cw_const_cast(type, expression) ((type)(expression))
#	endif
#endif /* cw_const_cast */

#ifndef cw_declare
#	ifdef CW_CONFIG_USING_LANGUAGE_CPP
#		define cw_declare extern "C"
#	else
#		define cw_declare extern
#	endif
#endif /* cw_declare */

#ifndef cw_inline
#	if defined(CW_CONFIG_USING_LANGUAGE_CPP) || defined(CW_CONFIG_HAVE_C_99)
#		define cw_inline inline
#	elif defined(CW_CONFIG_USING_COMPILER_GNU)
#		define cw_inline __inline__
#	elif defined(CW_CONFIG_USING_COMPILER_MSVC)
#		define cw_inline __inline
#	else
#		define cw_inline
#	endif
#endif /* cw_inline */

#ifndef cw_nodiscard
#	if defined(CW_CONFIG_HAVE_CPP_17)
#		define cw_nodiscard [[nodiscard]]
#	elif defined(CW_CONFIG_USING_COMPILER_GNU)
#		define cw_nodiscard __attribute__((warn_unused_result))
#	elif defined(CW_CONFIG_USING_COMPILER_MSVC)
#		define cw_nodiscard _Check_return_
#	else
#		define cw_nodiscard
#	endif
#endif /* cw_nodiscard */

#ifndef cw_null
#	ifdef CW_CONFIG_USE_STANDARD_LIBRARY
#		include cw_libc_include(stddef)
#		if defined(CW_CONFIG_HAVE_CPP_11)
#			define cw_null nullptr
#		else
#			define cw_null NULL
#		endif
#	else
#		ifdef CW_CONFIG_USING_LANGUAGE_CPP
#			define cw_null 0
#		else
#			define cw_null ((void *)0)
#		endif
#	endif
#endif /* cw_null */

#ifndef cw_reinterpret_cast
#	ifdef CW_CONFIG_USING_LANGUAGE_CPP
#		define cw_reinterpret_cast(type, expression) reinterpret_cast<type>(expression)
#	else
#		define cw_reinterpret_cast(type, expression) ((type)(expression))
#	endif
#endif /* cw_reinterpret_cast */

#ifndef cw_restrict
#	if defined(CW_CONFIG_HAVE_C_99)
#		define cw_restrict restrict
#	elif defined(CW_CONFIG_USING_COMPILER_GNU)
#		define cw_restrict __restrict__
#	elif defined(CW_CONFIG_USING_COMPILER_MSVC)
#		define cw_restrict __restrict
#	else
#		define cw_restrict
#	endif
#endif /* cw_restrict */

#ifndef cw_static_assert
#	if defined(CW_CONFIG_HAVE_CPP_11)
#		define cw_static_assert(name, condition) static_assert(condition, #                name)
#	elif defined(CW_CONFIG_HAVE_C_11)
#		define cw_static_assert(name, condition) _Static_assert(condition, #                name)
#	else
#		define cw_static_assert(name, condition) typedef int cw_static_assert_##name[(condition)*2 - 1]
#	endif
#endif /* cw_static_assert */

#ifndef cw_static_cast
#	ifdef CW_CONFIG_USING_LANGUAGE_CPP
#		define cw_static_cast(type, expression) static_cast<type>(expression)
#	else
#		define cw_static_cast(type, expression) ((type)(expression))
#	endif
#endif /* cw_static_cast */

#endif /* CW_H */
