#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef BYTE_WIDTH
#	define BYTE_WIDTH 8
#endif /* BYTE_WIDTH */

#ifndef HAVE_LONG_LONG
#	if (defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L) || (defined(__cplusplus) && __cplusplus >= 201103L)
#		define HAVE_LONG_LONG
#	endif
#endif /* HAVE_LONG_LONG */

#ifndef MAXIMUM_BITS
#	define MAXIMUM_BITS 64
#endif /* MAXIMUM_BITS */

#ifndef MINIMUM_BITS
#	define MINIMUM_BITS 8
#endif /* MINIMUM_BITS */

#ifndef c_static_cast
#	ifdef __cplusplus
#		define c_reinterpret_cast(type, expression) reinterpret_cast<type>(expression)
#	else
#		define c_reinterpret_cast(type, expression) ((type)(expression))
#	endif
#endif /* c_static_cast */

static const char *get_type_for(unsigned int bits, int is_signed);

static const char *
get_type_for(const unsigned int bits, const int is_signed)
{
	unsigned int bytes;

	assert(bits >= BYTE_WIDTH && bits % BYTE_WIDTH == 0 && is_signed >= 0 && is_signed <= 1);
	bytes = bits / BYTE_WIDTH;
	if (is_signed && sizeof(signed char) == bytes)
		return "signed char";
	if (is_signed && sizeof(signed short) == bytes)
		return "signed short";
	if (is_signed && sizeof(signed int) == bytes)
		return "signed int";
	if (is_signed && sizeof(signed long) == bytes)
		return "signed long";
#ifdef HAVE_LONG_LONG
	if (is_signed && sizeof(signed long long) == bytes)
		return "signed long long";
#endif /* HAVE_LONG_LONG */
	if (sizeof(unsigned char) == bytes)
		return "unsigned char";
	if (sizeof(unsigned short) == bytes)
		return "unsigned short";
	if (sizeof(unsigned int) == bytes)
		return "unsigned int";
	if (sizeof(unsigned long) == bytes)
		return "unsigned long";
#ifdef HAVE_LONG_LONG
	if (sizeof(unsigned long long) == bytes)
		return "unsigned long long";
#endif /* HAVE_LONG_LONG */
	return NULL;
}

int
main(void)
{
	unsigned int bits;
	const volatile unsigned int endian = 0x1U;
	int is_signed;
	const char *type;

	(void)fprintf(stdout, "#ifndef CW_ARCH_H\n#define CW_ARCH_H\n\n");
	if (*c_reinterpret_cast(const volatile unsigned char *, &endian) == 0x1U) {
		(void)fprintf(stdout, "#undef CW_ARCH_IS_BIG_ENDIAN\n");
		(void)fprintf(stdout, "#define CW_ARCH_IS_LITTLE_ENDIAN\n");
	} else {
		(void)fprintf(stdout, "#define CW_ARCH_IS_BIG_ENDIAN\n");
		(void)fprintf(stdout, "#undef CW_ARCH_IS_LITTLE_ENDIAN\n");
	}
	for (is_signed = 1; is_signed >= 0; --is_signed) {
		for (bits = MINIMUM_BITS; bits <= MAXIMUM_BITS; bits *= 2) {
			if ((type = get_type_for(bits, is_signed)) == NULL) {
				(void)fprintf(stderr, "Could not find a valid type for CW_ARCH_TYPE_FOR_%s_%u_BITS.\n", is_signed ? "SIGNED" : "UNSIGNED", bits);
				return EXIT_FAILURE;
			}
			(void)fprintf(stdout, "#define CW_ARCH_TYPE_FOR_%s_%u_BITS %s\n", is_signed ? "SIGNED" : "UNSIGNED", bits, type);
		}
	}
	if ((type = get_type_for(MAXIMUM_BITS, 0)) == NULL) {
		(void)fprintf(stderr, "Could not find a valid type for CW_ARCH_TYPE_FOR_SIZE.\n");
		return EXIT_FAILURE;
	}
	(void)fprintf(stdout, "#define CW_ARCH_TYPE_FOR_SIZE %s\n", type);
	if ((type = get_type_for(sizeof("abc" - "123") * BYTE_WIDTH, 1)) == NULL) { /* NOLINT(bugprone-sizeof-expression) */
		(void)fprintf(stderr, "Could not find a valid type for CW_ARCH_TYPE_FOR_PTRDIFF.\n");
		return EXIT_FAILURE;
	}
	(void)fprintf(stdout, "#define CW_ARCH_TYPE_FOR_PTRDIFF %s\n", type);
	if ((type = get_type_for(sizeof(void *) * BYTE_WIDTH, 1)) == NULL) {
		(void)fprintf(stderr, "Could not find a valid type for CW_ARCH_TYPE_FOR_SPTR.\n");
		return EXIT_FAILURE;
	}
	(void)fprintf(stdout, "#define CW_ARCH_TYPE_FOR_SPTR %s\n", type);
	if ((type = get_type_for(sizeof(void *) * BYTE_WIDTH, 0)) == NULL) {
		(void)fprintf(stderr, "Could not find a valid type for CW_ARCH_TYPE_FOR_UPTR.\n");
		return EXIT_FAILURE;
	}
	(void)fprintf(stdout, "#define CW_ARCH_TYPE_FOR_UPTR %s\n", type);
	(void)fprintf(stdout, "\n#endif /* CW_ARCH_H */\n");
	return EXIT_SUCCESS;
}
