#ifndef CW_STRING_H
#define CW_STRING_H

#include "cw.h"

cw_declare cw_nodiscard int cw_call cw_string_compare(const char *left, const char *right);
cw_declare cw_nodiscard char *cw_call cw_string_find_right_char(char *string, char character);
cw_declare cw_nodiscard const char *cw_call cw_string_find_right_char_const(const char *string, char character);
cw_declare cw_nodiscard cw_size cw_call cw_string_length(const char *string);

#endif /* CW_STRING_H */
