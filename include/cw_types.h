#ifndef CW_TYPES_H
#define CW_TYPES_H

#include "cw_arch.h"
#include "cw_libc.h"

#ifdef CW_CONFIG_USE_STANDARD_LIBRARY
#	include cw_libc_include(stddef)
typedef cw_libc_load(ptrdiff_t) cw_ptrdiff;
typedef cw_libc_load(size_t) cw_size;
#else
typedef CW_ARCH_TYPE_FOR_PTRDIFF cw_ptrdiff;
typedef CW_ARCH_TYPE_FOR_SIZE cw_size;
#endif /* CW_CONFIG_USE_STANDARD_LIBRARY */

#if defined(CW_CONFIG_USE_STANDARD_LIBRARY) && (defined(CW_CONFIG_HAVE_CPP_11) || defined(CW_CONFIG_HAVE_C_99))
#	include cw_libc_include(stdint)
typedef cw_libc_load(int8_t) cw_s8;
typedef cw_libc_load(int16_t) cw_s16;
typedef cw_libc_load(int32_t) cw_s32;
typedef cw_libc_load(int64_t) cw_s64;
typedef cw_libc_load(intptr_t) cw_sptr;
typedef cw_libc_load(uint8_t) cw_u8;
typedef cw_libc_load(uint16_t) cw_u16;
typedef cw_libc_load(uint32_t) cw_u32;
typedef cw_libc_load(uint64_t) cw_u64;
typedef cw_libc_load(uintptr_t) cw_uptr;
#else
typedef CW_ARCH_TYPE_FOR_SIGNED_8_BITS cw_s8;
typedef CW_ARCH_TYPE_FOR_SIGNED_16_BITS cw_s16;
typedef CW_ARCH_TYPE_FOR_SIGNED_32_BITS cw_s32;
typedef CW_ARCH_TYPE_FOR_SIGNED_64_BITS cw_s64;
typedef CW_ARCH_TYPE_FOR_SPTR cw_sptr;
typedef CW_ARCH_TYPE_FOR_UNSIGNED_8_BITS cw_u8;
typedef CW_ARCH_TYPE_FOR_UNSIGNED_16_BITS cw_u16;
typedef CW_ARCH_TYPE_FOR_UNSIGNED_32_BITS cw_u32;
typedef CW_ARCH_TYPE_FOR_UNSIGNED_64_BITS cw_u64;
typedef CW_ARCH_TYPE_FOR_UPTR cw_uptr;
#endif /* defined(CW_CONFIG_USE_STANDARD_LIBRARY) && (defined(CW_CONFIG_HAVE_CPP_11) || defined(CW_CONFIG_HAVE_C_99)) */

#endif /* CW_TYPES_H */
