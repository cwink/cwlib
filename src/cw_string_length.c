#include "cw_string.h"

#ifdef CW_CONFIG_USE_STANDARD_LIBRARY
#	include cw_libc_include(string)
#endif /* CW_CONFIG_USE_STANDARD_LIBRARY */

cw_size
cw_string_length(const char *const string)
{
#ifndef CW_CONFIG_USE_STANDARD_LIBRARY
	const char *ptr;
#endif /* CW_CONFIG_USE_STANDARD_LIBRARY */

	cw_assert(string != cw_null);
#ifndef CW_CONFIG_USE_STANDARD_LIBRARY
	for (ptr = string; *ptr != '\0'; ++ptr)
		;
	return ptr - string;
#else
	return cw_libc_load(strlen)(string);
#endif /* CW_CONFIG_USE_STANDARD_LIBRARY */
}
