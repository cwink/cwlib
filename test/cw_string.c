#include "cw_string.h"
#include "cw_test.h"

static void
compare(struct cw_test *const test)
{
	cw_test_assert(test, cw_string_compare("", "") == 0);
	cw_test_assert(test, cw_string_compare("a", "a") == 0);
	cw_test_assert(test, cw_string_compare("ab", "ab") == 0);
	cw_test_assert(test, cw_string_compare("ab c", "ab c") == 0);
	cw_test_assert(test, cw_string_compare("a", "b") == -1);
	cw_test_assert(test, cw_string_compare("b", "a") == 1);
	cw_test_assert(test, cw_string_compare("ab", "bb") == -1);
	cw_test_assert(test, cw_string_compare("bb", "ab") == 1);
}

static void
find_right_char(struct cw_test *const test)
{
	char a[] = "", b[] = "/usr/local/src.d", c[] = "src.d";

	cw_test_assert(test, cw_string_find_right_char(a, '/') == cw_null);
	cw_test_assert(test, cw_string_find_right_char(a, '\0') == a);
	cw_test_assert(test, cw_string_find_right_char(b, '.') == (b + 14));
	cw_test_assert(test, cw_string_find_right_char(b, '/') == (b + 10));
	cw_test_assert(test, cw_string_find_right_char(c, '/') == cw_null);
}

static void
find_right_char_const(struct cw_test *const test)
{
	const char *a = "", *b = "/usr/local/src.d", *c = "src.d";

	cw_test_assert(test, cw_string_find_right_char_const(a, '/') == cw_null);
	cw_test_assert(test, cw_string_find_right_char_const(a, '\0') == a);
	cw_test_assert(test, cw_string_find_right_char_const(b, '.') == (b + 14));
	cw_test_assert(test, cw_string_find_right_char_const(b, '/') == (b + 10));
	cw_test_assert(test, cw_string_find_right_char_const(c, '/') == cw_null);
}

static void
length(struct cw_test *const test)
{
	cw_test_assert(test, cw_string_length("") == 0);
	cw_test_assert(test, cw_string_length("a") == 1);
	cw_test_assert(test, cw_string_length("abc") == 3);
	cw_test_assert(test, cw_string_length("abc 123") == 7);
}

int
main(int argc, char *argv[])
{
	struct cw_test test;

	cw_test_initialize(&test, argc, argv);
	cw_test_add(&test, compare, "Test for cw_string_compare.");
	cw_test_add(&test, find_right_char, "Test for cw_string_find_right_char.");
	cw_test_add(&test, find_right_char_const, "Test for cw_string_find_right_char_const.");
	cw_test_add(&test, length, "Test for cw_string_length.");
	cw_test_run(&test);
	return test.exit_code;
}
