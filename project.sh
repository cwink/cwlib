#!/bin/sh

# shellcheck disable=SC2086

ARFLAGS="${ARFLAGS} rcs"
ASAN_OPTIONS="strict_string_checks=1:detect_stack_use_after_return=1:check_initialization_order=1:strict_init_order=1"
CFLAGS="${CFLAGS} -Iinclude"
LDFLAGS="${LDFLAGS} -L."
LDLIBS="${LDLIBS} -lcw"
UBSAN_OPTIONS="print_stacktrace=1"

check_for_program() {
	if ! command -v "${1}" >/dev/null; then
		print_error "Cannot run required program %s." "${1}"
		return 1
	fi
	return 0
}

print_error() {
	# shellcheck disable=SC2059
	printf "$@" 1>&2
	printf "\n" 1>&2
	return 0
}

trim_string() {
	echo "${1}" | sed "s/^[[:space:]]*//" | sed "s/[[:space:]]*$//"
	return 0
}

usage() {
	cat <<EOF
Usage: ./project.sh [-a] [-A arguments] [-b] [-c] [-d] [-h] [-l] [-r] [-s strategy] targets...

	The build script for the CWLib project.

	options:

		-a	Perform analysis on the given target (implies -b).
		-A	Adds command line arguments to the running target (implies -r).
		-b	Build the given target.
		-c	Clean the given target.
		-d	Enable debugging/assertions.
		-h	Shows this help information.
		-l	Use the standard library with the target builds.
		-r	Runs the given target (implies -b).
		-s	Sanitize the given target using the given strategy (implies -b and -d).

	targets:

		all	The entire project.
		apps	All apps in the app directory.
		file	A source file (full path).
		lib	Just the library.
		tests	All tests in the test directory.

	environment variables:

		AR		The library archiver to use.
		ARFLAGS		Any additional library archiver flags to use.
		CC		The C/C++ compiler to use.
		CLANG		The name and/or path for clang.
		CLANG_TIDY	The name and/or path for clang-tidy.
		CFLAGS		Any additional compiler flags to use.
		LDFLAGS		Any additional library flags to use.
		LDLIBS		Any additional libraries to use.
		MAKE		The name and/or path for make.

	sanitization strategies:

		asan	Address sanitization.
		ubsan	Undefined behavior sanitization.
EOF
}

ANALYZE_TARGET=0
BUILD_TARGET=0
CLEAN_TARGET=0
DEBUG_TARGET=0
RUN_ARGUMENTS=""
RUN_TARGET=0
SANITIZE_STRATEGY=""

while getopts "aA:bcdhlrs:" OPTION; do
	case "${OPTION}" in
	"a")
		ANALYZE_TARGET=1
		BUILD_TARGET=1
		;;
	"A")
		RUN_TARGET=1
		;;
	"b")
		BUILD_TARGET=1
		;;
	"c")
		CLEAN_TARGET=1
		;;
	"d")
		DEBUG_TARGET=1
		;;
	"l")
		CFLAGS="${CFLAGS} -DCW_CONFIG_USE_STANDARD_LIBRARY"
		;;
	"r")
		BUILD_TARGET=1
		RUN_ARGUMENTS="${OPTARG}"
		RUN_TARGET=1
		;;
	"s")
		BUILD_TARGET=1
		DEBUG_TARGET=1
		SANITIZE_STRATEGY="$(trim_string "${OPTARG}")"
		;;
	"?")
		printf "\n"
		usage
		exit 1
		;;
	*)
		usage
		exit 0
		;;
	esac
done

shift $((OPTIND - 1))

[ "${AR}" = "" ] && AR="ar"
[ "${CC}" = "" ] && CC="gcc"
[ "${CLANG}" = "" ] && CLANG="clang"
[ "${CLANG_TIDY}" = "" ] && CLANG_TIDY="clang-tidy"
[ "${MAKE}" = "" ] && MAKE="make"

check_for_program "${AR}" || exit 1
check_for_program "${CC}" || exit 1
check_for_program "${CLANG}" || exit 1
check_for_program "${CLANG_TIDY}" || exit 1
check_for_program "${MAKE}" || exit 1

export AR ARFLAGS ASAN_OPTIONS CC CFLAGS CLANG LDFLAGS LDLIBS UBSAN_OPTIONS

APP_FILES="$(find "app" -type f -name "*.c" -exec echo "{}" +)" || exit 1
APP_FILES_BIN="$(echo "${APP_FILES}" | sed "s/\.c/.x/g")"
APP_FILES_JSON="$(echo "${APP_FILES}" | sed "s/\.c/.json/g")"
APP_FILES_OBJ="$(echo "${APP_FILES}" | sed "s/\.c/.o/g")"
SRC_FILES="$(find "src" -type f -name "*.c" -exec echo "{}" +)" || exit 1
SRC_FILES_JSON="$(echo "${SRC_FILES}" | sed "s/\.c/.json/g")"
SRC_FILES_OBJ="$(echo "${SRC_FILES}" | sed "s/\.c/.o/g")"
TEST_FILES="$(find "test" -type f -name "*.c" -exec echo "{}" +)" || exit 1
TEST_FILES_BIN="$(echo "${TEST_FILES}" | sed "s/\.c/.x/g")"
TEST_FILES_JSON="$(echo "${TEST_FILES}" | sed "s/\.c/.json/g")"
TEST_FILES_OBJ="$(echo "${TEST_FILES}" | sed "s/\.c/.o/g")"

SRC_FILES_BIN="$(echo "${SRC_FILES_OBJ}" | sed "s/\([^ ]*\)/libcw.a(\1)/g")"

if [ "${DEBUG_TARGET}" -eq 1 ]; then
	CFLAGS="${CFLAGS} -g -O0 -DCW_CONFIG_ENABLE_ASSERTIONS"
fi

if [ "${SANITIZE_STRATEGY}" != "" ]; then
	SANITIZE_STRATEGY_LOWER="$(echo "${SANITIZE_STRATEGY}" | tr "[:upper:]" "[:lower:]")"
	case "${SANITIZE_STRATEGY_LOWER}" in
	"asan")
		CFLAGS="${CFLAGS} -fsanitize=address,pointer-compare,pointer-subtract -fsanitize-address-use-after-scope -fno-omit-frame-pointer"
		LDFLAGS="${LDFLAGS} -fsanitize=address,pointer-compare,pointer-subtract -fsanitize-address-use-after-scope -fno-omit-frame-pointer"
		;;
	"ubsan")
		CFLAGS="${CFLAGS} -fsanitize=undefined,signed-integer-overflow -fsanitize-address-use-after-scope -fno-omit-frame-pointer"
		LDFLAGS="${LDFLAGS} -fsanitize=undefined,signed-integer-overflow -fsanitize-address-use-after-scope -fno-omit-frame-pointer"
		;;
	*)
		print_error "%s is an invalid sanitization strategy." "${SANITIZE_STRATEGY}"
		exit 1
		;;
	esac
fi

while [ "$#" -ne 0 ]; do
	TARGET="$(trim_string "${1}")"

	if [ "${TARGET}" = "" ]; then
		usage
		exit 0
	fi

	TARGET_BASE="${TARGET%%.*}"
	TARGET_LOWER="$(echo "${TARGET}" | tr "[:upper:]" "[:lower:]")"

	if [ "${CLEAN_TARGET}" -eq 1 ]; then
		case "${TARGET_LOWER}" in
		"all")
			rm -f "libcw.a" "compile_commands.json" ${APP_FILES_BIN} ${APP_FILES_JSON} ${APP_FILES_OBJ} ${SRC_FILES_JSON} ${SRC_FILES_OBJ} ${TEST_FILES_BIN} ${TEST_FILES_JSON} ${TEST_FILES_OBJ}
			;;
		"apps")
			rm -f "compile_commands.json" ${APP_FILES_BIN} ${APP_FILES_JSON} ${APP_FILES_OBJ}
			;;
		"lib")
			rm -f "libcw.a" "compile_commands.json" ${SRC_FILES_JSON} ${SRC_FILES_OBJ}
			;;
		"tests")
			rm -f "compile_commands.json" ${TEST_FILES_BIN} ${TEST_FILES_JSON} ${TEST_FILES_OBJ}
			;;
		*)
			if [ ! -f "${TARGET}" ]; then
				print_error "File %s is not a valid file." "${TARGET}"
				exit 1
			fi
			rm -f "compile_commands.json" "${TARGET_BASE}.x" "${TARGET_BASE}.o" "${TARGET_BASE}.json"
			;;
		esac
	fi
	if [ "${BUILD_TARGET}" -eq 1 ]; then
		case "${TARGET_LOWER}" in
		"all")
			"${MAKE}" ${SRC_FILES_BIN} ${SRC_FILES_JSON} ${APP_FILES_BIN} ${APP_FILES_JSON} ${TEST_FILES_BIN} ${TEST_FILES_JSON} || exit 1
			;;
		"apps")
			"${MAKE}" ${APP_FILES_BIN} ${APP_FILES_JSON} || exit 1
			;;
		"lib")
			"${MAKE}" ${SRC_FILES_BIN} ${SRC_FILES_JSON} || exit 1
			;;
		"tests")
			"${MAKE}" ${TEST_FILES_BIN} ${TEST_FILES_JSON} || exit 1
			;;
		*)
			if [ ! -f "${TARGET}" ]; then
				print_error "File %s is not a valid file." "${TARGET}"
				exit 1
			fi
			"${MAKE}" "${TARGET_BASE}.x" "${TARGET_BASE}.json" || exit 1
			;;
		esac
	fi
	if [ "${ANALYZE_TARGET}" -eq 1 ]; then
		case "${TARGET_LOWER}" in
		"all")
			printf "[\n" >compile_commands.json
			cat ${APP_FILES_JSON} ${SRC_FILES_JSON} ${TEST_FILES_JSON} >>compile_commands.json
			printf "]\n" >>compile_commands.json
			"${CLANG_TIDY}" ${APP_FILES} ${SRC_FILES} ${TEST_FILES} || exit 1
			;;
		"apps")
			printf "[\n" >compile_commands.json
			cat ${APP_FILES_JSON} >>compile_commands.json
			printf "]\n" >>compile_commands.json
			"${CLANG_TIDY}" ${APP_FILES} || exit 1
			;;
		"lib")
			printf "[\n" >compile_commands.json
			cat ${SRC_FILES_JSON} >>compile_commands.json
			printf "]\n" >>compile_commands.json
			"${CLANG_TIDY}" ${SRC_FILES} || exit 1
			;;
		"tests")
			printf "[\n" >compile_commands.json
			cat ${TEST_FILES_JSON} >>compile_commands.json
			printf "]\n" >>compile_commands.json
			"${CLANG_TIDY}" ${TEST_FILES} || exit 1
			;;
		*)
			if [ ! -f "${TARGET}" ]; then
				print_error "File %s is not a valid file." "${TARGET}"
				exit 1
			fi
			printf "[\n" >compile_commands.json
			cat "${TARGET_BASE}.json" >>compile_commands.json
			printf "]\n" >>compile_commands.json
			"${CLANG_TIDY}" "${TARGET}" || exit 1
			;;
		esac
	fi
	if [ "${RUN_TARGET}" -eq 1 ]; then
		case "${TARGET_LOWER}" in
		"all")
			for file in ${APP_FILES_BIN} ${TEST_FILES_BIN}; do
				"${file}" "${RUN_ARGUMENTS}" || exit 1
			done
			;;
		"apps")
			for file in ${APP_FILES_BIN}; do
				"${file}" "${RUN_ARGUMENTS}" || exit 1
			done
			;;
		"lib") ;;
		"tests")
			for file in ${TEST_FILES_BIN}; do
				"${file}" "${RUN_ARGUMENTS}" || exit 1
			done
			;;
		*)
			if [ ! -f "${TARGET_BASE}.x" ]; then
				print_error "File %s is not a valid file." "${TARGET_BASE}.x"
				exit 1
			fi
			"${TARGET_BASE}.x" "${RUN_ARGUMENTS}" || exit 1
			;;
		esac
	fi
	shift || break
done

exit 0
