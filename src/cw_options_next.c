#include "cw_error.h"
#include "cw_options.h"

int
cw_options_next(struct cw_options *const options, const char **const cw_restrict argument, const char **const cw_restrict parameter)
{
	const char *ptr;

	cw_assert(options != cw_null && argument != cw_null && parameter != cw_null);
	*argument = *parameter = cw_null;
	if (options->index + 1 >= options->argc)
		return 0;
	++options->index;
	if (*options->argv[options->index] != '-') {
		options->argc = options->index;
		return 0;
	}
	*argument = options->argv[options->index] + 1;
	if (*(*argument + 1) != '\0')
		return -CW_ERROR_INVALID_ARGUMENT;
	for (ptr = options->arguments; *ptr != '\0' && *ptr != **argument; ++ptr)
		;
	if (*ptr == '\0')
		return -CW_ERROR_UNKNOWN_ARGUMENT;
	if (*(ptr + 1) == ':') {
		if (options->index + 1 >= options->argc)
			return -CW_ERROR_MISSING_PARAMETER;
		*parameter = options->argv[++options->index];
	}
	return 1;
}
